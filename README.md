# BreadTube API

The content management system and api for the BreadTube ecosystem. We store all the
relationships between Creators, Channels, Videos, and Playlists here and then
distribute that information to our website via an API ... allegedly.

This project is very new, so new that no code actually exists yet, issues are
being kept within [Gitlab/breadtube](https://gitlab.com/groups/breadtube/-/boards)

## Getting Started

First things first is we need to design this thing.

![UML](https://gitlab.com/breadtube/app.breadtube.tv/raw/master/diagrams/uml.png)

[Source: LucidChart](https://www.lucidchart.com/documents/edit/e6dae539-f274-47bd-8221-a941fdc8f436/0_0?beaconFlowId=E5DF734E9DD0576E)

### Creator

These are the people/groups/teams that make content, they generally receive financial support through something like Patreon.

### Channel

This is an area where a creator can publish their work, it is tied to a provider such as YouTube, or a PeerTube instance.

### Content

Creators have pieces of content which they put out in the world, that content may be a video, podcast, or something else. The content is distributed on the various channels the creators is one, one piece of content may exist on many channels.

### Playlist

Playlists allow users to collate content together in order to tell a story about a particular topic. Playlists can be related to a channel to show a specific creators set of content.

## Developing

This project uses the [Strapi](https://strapi.io) content management system.

### Prerequisites

- Node.js >= 10.x
- NPM >= 6.x
- PostgresSQL >= 10

### Installing

`yarn install`

### Database

`createdb breadtube_api`

`psql breadtube_api < data/development.dump`

### Local Server

`yarn develop`

http://localhost:1337/admin/auth/login

- Username: `admin`
- Password: `password`

### Database Dump

`pg_dump -f data/development.dump breadtube_api`

## Managing Content

### Content Types

Use lowercase for the field name

## Support

### API Documentation

http://localhost:1337/documentation/v1.0.0

## Contributing

Oh my, yes please do [CONTRIBUTING](https://gitlab.com/breadtube/app.breadtube.tv/blob/master/CONTRIBUTING.md)

## License

This project is licensed under the MIT License - see [LICENSE](https://gitlab.com/breadtube/app.breadtube.tv/blob/master/LICENSE)
