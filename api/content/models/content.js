const slugify = require('slugify');

module.exports = {
  beforeSave: async (model, attrs, options) => {
    if (options.method === 'insert' && attrs.name) {
      model.set('slug', slugify(attrs.name, { remove: /["~!@#$%^&*\(\)\-_+=`{}\[\]\|\\:;'<>,.\/?"]+/g, replacement: '', lower: true}));
    } else if (options.method === 'update' && attrs.name) {
      attrs.slug = slugify(attrs.name, { remove: /["~!@#$%^&*\(\)\-_+=`{}\[\]\|\\:;'<>,.\/?"]+/g, replacement: '', lower: true});
    }
  },
};
